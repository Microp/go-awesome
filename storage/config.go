package storage

import "fmt"

// Config конфигурация стораджа
type Config struct {
	Addr        string `toml:"address"`
	Username    string
	Password    string
	Database    string
	MaxOpenConn int `toml:"max_open_conn"`
	Debug       bool
}

// ToURL вернет URL для доступа к БД
func (s Config) ToURL() string {
	return fmt.Sprintf("postgresql://%s:%s@%s/%s?sslmode=disable", s.Username, s.Password, s.Addr, s.Database)
}
